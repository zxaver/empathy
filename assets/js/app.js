/**
 * Created by Zxaver on 7/21/2016.
 */
$(function () {
    $('#datepicker').datetimepicker({
        format: 'yyyy-mm-dd',
        startView: 'month',
        minView: 'month',
        autoclose: true
    });
});