<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
/**
 * Created by PhpStorm.
 * User: Zxaver
 * Date: 7/12/2016
 * Time: 3:25 PM
 */
class My_Controller extends CI_Controller
{
    
    public function __construct()
    {
        parent::__construct();

        if (!getLoggedInUserId())
        {
            redirect('auth/login');
        }
    }
    
    public function display($page = '', $data=array())
    {
        $data['content'] = $this->load->view($page, $data, true);
        $this->load->view('template/default', $data);
    }
}