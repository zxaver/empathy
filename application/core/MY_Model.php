<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
/**
 * Created by PhpStorm.
 * User: Zxaver
 * Date: 7/12/2016
 * Time: 3:24 PM
 */
class My_Model extends CI_Model
{
    protected $Data;

    public $tableName;

    protected $logTable;

    protected $postFieldMap;

    public $totaRows;

    public $currentPage;

    public $order_by;

    protected $errors = array();

    protected $flagIsUpdate=false;

    protected $pks = array();

    protected $autoInc = array();

    public function __construct()
    {
        parent::__construct();
    }


    public function get($key)
    {
        if (isset($this->Data[$key]))
            return $this->Data[$key];
        return false;

    }

    public function set($key, $val)
    {
        if (array_key_exists($key, $this->Data ))
             $this->Data[$key] = $val;

    }

    public function getDataFromPost()
    {
        foreach ($_POST as $key => $val) {
            if (isset($this->postFieldMap[$key]))    //html_field => db_field
            {
                $dbField = $this->postFieldMap[$key];
                $val = htmlentities($val, ENT_QUOTES);
                $this->Data[$dbField] = $val;
            }
        }
    }

    public function load($cond = array())
    {
        $retVal = false;
        if ($cond === null || !is_array($cond)) {
            return false;
        }

        $res = $this->searchDb($cond);
        if ($res) {
            $this->Data = 	$res[0];
            $this->flgIsUpdate = true;
            return true;
        }

        return false;
    }
    public function save()
    {

        if ($this->validate()) {
            return $this->setError("empty data can not be saved!");
        }
        $this->handleTimeStamp();
        if ($this->isUpdate()) {
            $pks = $this->getPKs();
            if (!$pks) {
                return $this->setError('Primary keys not set for update');    // Primary keys not set for update
            }

            $data = $this->getData();
            foreach ($pks as $key => $val) {
                unset($data[$key]);
            }
            $this->db->where($pks);
            return $this->db->update($this->mainTable, $data);
        }

        $res = $this->db->insert($this->tableName, $this->Data);
        if ($res)    // Save was successful
        {
            $lastInsertID = $this->db->insert_id();

            if ($lastInsertID) {
                $this->Data[$this->autoInc[0]] = $lastInsertID;
            }
            $this->flgIsUpdate = true;
            return true;
        }

        return $this->setError('Could not save -- unknown reason.');
    }

    public function getData()
    {
        return empty($this->Data)? false : $this->Data;
    }

    public function setData($newData)
    {
        $this->Data = $newData;

    }

    public function getRows($cond=array(), $limit = 10, $offset = 0)
    {
        $options = array('limit'=>$limit, 'offset'=> $offset);
        return $this->searchDb($cond, 'all', $options );
    }

    public function getRowByID($Id)
    {
        $cond = array('Id'=> "{$this->tableName}.Id");
        $result = $this->searchDb($cond);
        return $result ? $result[0] : false;
    }

    public function getRowByAnyField($cond = array())
    {
        $result = $this->searchDb($cond);
        return $result ? $result[0] : false;
    }

    public function getLatestRows($cond, $limit = 10, $offset = 0)
    {
        $options = array(
            'limit'=>$limit,
            'offset'=> $offset,
            'orderBy'=>"{$this->tableName}.created_at"
        );
        return $this->searchDb($cond, 'all', $options );
    }

    public function searchDb($cond, $fields = 'all',$options = array())
    {
        $fields = $fields == 'all' ? '*' : $fields;
        
        $this->db->select($fields)->from($this->tableName)->where($cond);

        if (array_key_exists("offset", $options) && array_key_exists("limit", $options))
        {
            $this->db->limit($options['offset'], $options['limit']);
        }

        elseif (!array_key_exists("offset", $options) && array_key_exists("limit", $options))
        {
            $this->db->limit($options['limit']);
        }

        if (array_key_exists("orderBy", $options))
        {
            $this->db->order_by($options['orderBy']);
        }

        return $this->db->get()->result_array();
    }

    public function getPKs()
    {
        $arr = array();

        foreach ($this->pk as $pk) {
            if (isset($this->Data[$pk])) {
                $arr[$pk] = $this->Data[$pk];
            }
        }
        return $arr;
    }

    public function handleTimeStamp()
    {
        if($this->isUpdate() && array_key_exists("updated_at", $this->Data))
        {
            $this->Data['updated_at']= date('Y-m-d H:i:s', time());

        }
        elseif(array_key_exists("updated_at", $this->Data))
        {
            $this->Data['created_at']= date("Y-m-d H:i:s", time());

        }
    }


    public function delete()
    {


    }

    public function isUpdate()
    {
       return $this->flagIsUpdate;
    }

    public function setError($error)
    {
        array_push($this->errors, $error);
        return false;
    }

    public function getError()
    {
        return array_pop($this->errors);
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function validate()
    {
        if (!$this->Data)        // Data is blank
        {
            return false;
        }

        $flgError = 1;
        foreach ($this->Data as $key => $val) {
            if ($val != null) {
                $flgError = false;
            }
        }
        return $flgError;
    }

}