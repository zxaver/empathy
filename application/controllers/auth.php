<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
/**
 * Created by PhpStorm.
 * User: Zxaver
 * Date: 7/12/2016
 * Time: 4:52 PM
 */
class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        redirect('auth/login');
    }

    public function login()
    {
        $this->load->helper('password');
        $this->load->model('user_model','user');
        $this->load->model('auth_model', 'auth');
        $this->form_validation->set_rules('email', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if($this->form_validation->run()== false)
        {
            $data['error'] = validation_errors();
            $this->load->view('pages/user/login', $data);
        }
        else
        {
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $user = $this->validate($email, $password);
            if ($this->validate($email, $password))
            {
                $userData = $this->user->getData();
                $this->auth->saveSessionData($userData,'logged_in');
                redirect('/');
            }
            else
            {
                $data['error'] = 'email and password mismatch.';
                $this->load->view('pages/user/login', $data);
            }
        }
    }

    protected function validate($email, $password)
    {
        if ($this->user->load(array('email'=>$email)))
        {
            $salt = $this->user->get('salt');
            $password = encryptPassword($password, $salt);
            echoPre($salt);
            echoPre($password);
            return $this->authenticate($email, $password);
        }
        return false;
    }

    protected function authenticate($email, $password)
    {
        return $this->user->load(array('email'=>$email, 'password'=>$password));
    }


    public function logout()
    {
        $sess_array = array('username' => '');
        $this->session->unset_userdata('logged_in', $sess_array);
        redirect('/');
    }

}
