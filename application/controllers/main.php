<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
/**
 * Created by PhpStorm.
 * User: Zxaver
 * Date: 7/12/2016
 * Time: 4:49 PM
 */

class Main extends My_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        redirect('main/book');
    }

    public function book()
    {
        $this->load->model('station_model', 'station');
        //$this->load->model('ticket_model', 'ticket');
        $stations = $this->station->getRows();
        //echoPre($stations);
        $data['stations'] = $stations;
        $this->display('tickets/book', $data);

    }

    public function viewTickets()
    {
        $this->load->model('schedule_model', 'schedule');
        $this->load->model('ticket_model', 'ticket');
        $date = $this->input->post('date');
        $from = $this->input->post('from');
        $to = $this->input->post('to');
        //if schedule is available then show the tickets
        if($this->schedule->check($date, $from, $to))
        {
            $sch_id = $this->schedule->get('Id');
            $vehicle_type = $this->schedule->get('vehicle_type');
            $data['booked_tickets'] = $this->ticket->loadBooked($sch_id);
            $data['available_tickets'] = $this->ticket->getAvailable($sch_id, $vehicle_type);
            $this->display('tickets/view_tickets', $data);
        }
        else
        {
            //$this->session->flash_data('No route available');
            $this->display('tickets/book');
        }

    }
}