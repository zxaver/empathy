<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
/**
 * Created by PhpStorm.
 * User: Zxaver
 * Date: 7/12/2016
 * Time: 4:52 PM
 */
class User extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $this->load->helper('password');
    }

    public function index($msg = NULL)
    {
       redirect('auth/login');
    }

    protected function isUserExist()
    {
        $inputEmail = $this->input->post('email');
        return $this->user->load(array('email' => $inputEmail)) ? true : false;
    }


    public function signup()
    {
        $this->load->view('pages/user/signup');
    }

    public function create()
    {

        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
        $this->form_validation->set_rules('repassword', 'Confirm Password', 'required|matches[password]');

        $this->form_validation->set_message('required', 'The %s field is required.');
        $this->form_validation->set_message('min_length', 'Password must contain at least 6 characters');
        $this->form_validation->set_message('matches', 'Confirm password must matches with password');


        if ($this->form_validation->run() == FAlSE)
        {
            $data['error'] = validation_errors();
            $data['title'] = 'Welcome';
            $this->load->view('pages/user/signup', $data);
        }
        elseif ($this->isUserExist())
        {
            $data['error'] = "Email already exist";
            $data['title'] = 'Welcome';
            $this->load->view('pages/user/signup', $data);
        }
        else
        {
            $this->user->getDataFromPost();
            $data = $this->user->getData();
            $salt = getSalt();
            $this->user->set('password', encryptPassword($data['password'], $salt));
            $this->user->set('salt', $salt);
            if($this->user->save())
            {
                redirect('/');
            }
            else
            {
                echoPre($this->user->getErrors());
            }
        }
    }

}
