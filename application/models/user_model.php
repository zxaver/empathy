<?php
/**
 * Created by PhpStorm.
 * User: Zxaver
 * Date: 7/14/2016
 * Time: 4:07 PM
 */
Class User_model extends  My_Model
{
    
    public function __construct()
    {
        parent::__construct();
        $this->init();
    }

    public function init()
    {
        $this->tableName = 'users';
        $this->pks = array('Id');
        $this->autoInc = array('Id');
        $this->logTable = 'users_log';
        $dbFields = array(
        'Id',
        'email',
        'mobile',
        'first_name',
        'last_name',
        'usergroup_id',
        'created_at',
        'updated_at',
        'last_login_at',
        'isActive',
        'password',
        'salt'
        );

        $this->postFieldMap = array(
            // 'HTML field' =>  'DB_field'
            'password' => 'password',
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'mobile' =>  'mobile',
            'email' => 'email',
            'address' => 'address',
        );


        foreach ($dbFields as $key)
        {
            $this->Data[$key] = null;
        }
    }

    public function validate()
    {
        if (!$this->Data)        // Data is blank
        {
            return false;
        }

        $requiredFields = array('email','first_name','last_name','usergroup_id');
        foreach ($requiredFields as $key) {
            if ($this->Data[$key] == null) {
                return false;
            }
        }
        return true;
    }

}