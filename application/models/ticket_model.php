<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
/**
 * Created by PhpStorm.
 * User: Zxaver
 * Date: 7/25/2016
 * Time: 9:07 PM
 */

class ticket_model extends My_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->init();
    }

    protected function init()
    {
        $this->tableName = 'tickets';
        $this->logTable = 'tickets_log';
        $this->pks = array('Id');
        $this->autoInc = array('Id');

        $dbFields = array(
            'Id',
            'ticket_no',
            'created_at',
            'isActive',
            'status',
            'schedule_id'
        );
    }

    public function check($date, $from, $to)
    {
        $date = date('Y-m-d',strtotime($date));
        $cond = array(
            'date'=>$date,
            'from_station_id'=>$from,
            'to_station_id'=> $to
        );
        return $this->load($cond);
    }

    public function loadBooked($sch_id)
    {
        $this->db->from($this->logTable);
        $this->db->where(array('schedule_id'=>$sch_id,'status'=>3));
        return $this->db->get()->result_array();
    }

    public function getAvailable($sch_id, $vehicle_type)
    {
        $vehicle_details = $this->getVehicleDetails($vehicle_type);
        $total_seats = $vehicle_details['total_seats'];
        $booked_seats = count($this->loadBooked($sch_id));
        $available_seats = $total_seats-$booked_seats;
        return $available_seats > 0 ? $available_seats : false;
    }

    public function isAvailable($sch_id, $vehicle_type)
    {
        $vehicle_details = $this->getVehicleDetails($vehicle_type);
        $total_seats = $vehicle_details['total_seats'];
        $booked_seats = count($this->loadBooked($sch_id));
        $available_seats = $total_seats-$booked_seats;
        return $available_seats > 0 ? true : false;
    }

    public function getVehicleDetails($vehicle_type, $vehicle_id=null)
    {
        $this->db->from('vehicle_types');
        $this->db->where(array('id'=>$vehicle_type));
        return $this->db->get()->row_array();
    }

}