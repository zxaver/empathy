<?php
/**
 * Created by PhpStorm.
 * User: Zxaver
 * Date: 7/21/2016
 * Time: 3:33 PM
 */
class Station_model extends My_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->init();
    }

    protected function init()
    {
        $this->tableName = 'stations';
        $this->pks = array('Id');
        $this->autoInc = array('Id');

        $dbFields = array(
            'Id',
            'name',
            'type',
            'district',
            'created_at',
            'updated_at',
            'isActive',
        );

        $this->postFieldMap = array(
            // 'HTML field' =>  'DB_field'
            'name' => 'name',
            'type' => 'type',
            'district'=> 'district',
            'isActive' => 'isActive',
        );
    }


}