/**
 * Created by PhpStorm.
 * User: Zxaver
 * Date: 7/15/2016
 * Time: 3:39 PM
 */
<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Vechile_model extends My_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->init();
    }

    public function init()
    {
        $this->tableName = 'vehicles';
        $this->pks = array('Id');
        $this->autoInc = array('Id');
        $this->logTable = 'vehicles_log';
        $dbFields = array(
            'Id',
            'vehicle_no',
            'vehicle_model',
            'vehicle_brand',
            'engine_no',
            'chesis_no',
            'created_at',
            'updated_at',
            'isActive',
            'description',
            'seats'
        );

        $this->postFieldMap = array(
            // 'HTML field' =>  'DB_field'
            'password' => 'password',
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'mobile' =>  'mobile',
            'email' => 'email',
            'address' => 'address',
        );


        foreach ($dbFields as $key)
        {
            $this->Data[$key] = null;
        }
    }


}