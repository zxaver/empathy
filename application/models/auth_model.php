<?php
/**
 * Created by PhpStorm.
 * User: Zxaver
 * Date: 7/13/2016
 * Time: 2:55 PM
 */
Class Auth_model extends  My_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function validate()
    {

    }

    public function saveSessionData($userData=array(), $name='')
    {
        //$userData= array('userId', 'firstName', 'lastName', 'email', 'isActive', 'last_login_at');
        $this->session->set_userdata($name, $userData);
    }

}