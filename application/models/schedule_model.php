<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
/**
 * Created by PhpStorm.
 * User: Zxaver
 * Date: 7/25/2016
 * Time: 9:07 PM
 */

class schedule_model extends My_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->init();
    }

    protected function init()
    {
        $this->tableName = 'schedules';
        $this->logTable = 'schedule_log';
        $this->pks = array('Id');
        $this->autoInc = array('Id');

        $dbFields = array(
            'Id',
            'date',
            'from_staion_id',
            'to_station_id',
            'created_at',
            'updated_at',
            'created_by',
            'acted_by',
            'updated_by',
            'vehicle_type',
            'vehicle_id',
        );
    }

    public function check($date, $from, $to)
    {
        $date = date('Y-m-d',strtotime($date));
        $cond = array(
            'date'=>$date,
            'from_station_id'=>$from,
            'to_station_id'=> $to
        );
        //echoPre($cond);
        return $this->load($cond);
    }

}