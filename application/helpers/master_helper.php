<?php
/**
 * Created by PhpStorm.
 * User: Zxaver
 * Date: 7/13/2016
 * Time: 3:02 PM
 */

function echoPre($var, $returnText = false, $die = false)
{
    if ($returnText) {
        return print_r($var, true);
    }

    echo '<pre>', print_r($var, true), '</pre>';
    if ($die) {
        die;
    }
}

function getLoggedInUserId()
{
    $ci = 	&get_instance();
    if (isset($ci->session->userdata['_remember_me'])) {
        return $ci->session->userdata['_remember_me']['userid'];
    }
    elseif(isset($ci->session->userdata['logged_in'])) {
        return $ci->session->userdata['logged_in']['Id'];
    }
    else
        return false;

}