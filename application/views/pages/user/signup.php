<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $title ?> | Empathy</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url() ?>plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url() ?>plugins/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url() ?>assets/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url() ?>plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Please fill the details </h3>
                </div>
                <div class="panel-body">
                    <?php if (isset($error)):?>
                        <div class="alert alert-danger"><p><?php echo $error ?></p></div>
                    <?php endif ?>
                    <?php
                    $attributes = array('name'=>'user-signup', 'class'=>'input-form');
                    echo form_open('user/create', $attributes);
                    ?>
                    <fieldset>
                        <div class="form-group">
                            <?php  $data = array(
                                'name'          => 'email',
                                'placeholder'   => 'Email',
                                'value'         =>  '',
                                'class'         => 'form-control'
                            );
                            echo form_input($data)
                            ?>
                        </div>
                        <div class="form-group">
                            <?php  $data = array(
                                'name'          => 'first_name',
                                'placeholder'   => 'First Name',
                                'value'         =>  '',
                                'class'         => 'form-control'
                            );
                            echo form_input($data)
                            ?>
                        </div>
                        <div class="form-group">
                            <?php  $data = array(
                                'name'          => 'last_name',
                                'placeholder'   => 'Last Name',
                                'value'         =>  '',
                                'class'         => 'form-control'
                            );
                            echo form_input($data)
                            ?>
                        </div>
                        <div class="form-group">
                            <?php  $data = array(
                                'name'          => 'password',
                                'placeholder'   => 'Password',
                                'value'         =>  '',
                                'class'         => 'form-control'
                            );
                            echo form_password($data)
                            ?>
                        </div>
                        <div class="form-group">
                            <?php  $data = array(
                                'name'          => 'repassword',
                                'placeholder'   => 'Retype Password',
                                'value'         =>  '',
                                'class'         => 'form-control'
                            );
                            echo form_password($data)
                            ?>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input name="remember" type="checkbox" value="Remember Me">Remember Me
                            </label>
                        </div>
                        <input type="submit" class="btn btn-lg btn-success btn-block" value="Sign up" />
                    </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- jQuery -->
<script src="<?php echo base_url() ?>plugins/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url() ?>plugins/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url() ?>plugins/metisMenu/dist/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url() ?>assets/js/sb-admin-2.js"></script>

</body>

</html>