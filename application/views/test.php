<div class="alert alert-danger"><p>username or password missing</p></div>
<form action="/auth/showInput" method="POST" class="input-form" accept-charset="utf-8">
    <fieldset>
        <div class="form-group">
            <input class="form-control" placeholder="E-mail" name="email" type="email" />
        </div>
        <div class="form-group">
            <input class="form-control" placeholder="Password" name="password" type="password" />
        </div>
        <div class="checkbox">
            <label>
                <input name="remember" type="checkbox" value="Remember Me">Remember Me
            </label>
        </div>
        <input type="submit" class="btn btn-lg btn-success btn-block" value="Login" />
    </fieldset>
</form>