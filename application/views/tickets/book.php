<div id="page-wrapper">
    <div class="row">
        <?php echo form_open('main/viewTickets') ?>
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="form-group">
                <?php
                $date = array(
                    'name' => 'date',
                    'id' => 'datepicker',
                    'value' => '',
                    'class' => 'form-control required',
                );
                echo form_label('Date', 'date');
                echo form_input($date);
                ?>
            </div>
        </div>
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="form-group">
                <select name="from" class="form-control">
                    <?php
                    if (count($stations) > 0) {
                        foreach ($stations as $station) {
                            echo "<option value='" . $station['Id'] . "'>" . $station['name'] . "</option>";
                        }
                    }
                    ?>
                </select>
            </div>
        </div>

        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="form-group">
                <select name="to" class="form-control">
                    <?php
                    if (count($stations) > 0) {
                        foreach ($stations as $station) {
                            echo "<option value='" . $station['Id'] . "'>" . $station['name'] . "</option>";
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Proceed" />
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>