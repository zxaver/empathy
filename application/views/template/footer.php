<div class="footer">
</div>
</div> <!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url() ?>plugins/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url() ?>plugins/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url() ?>plugins/metisMenu/dist/metisMenu.min.js"></script>
<!-- Jquery Validation Plugin JavaScript -->
<script src="<?php echo base_url() ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url() ?>plugins/date-picker/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/app.js"></script>

<?php
if (isset($js)) {
    foreach ($js as $file) {
        echo '<script src="' . base_url() . $file . '" title="custom-scripts"></script>';
        echo "\n\t";
    }
}
?>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url() ?>assets/js/sb-admin-2.js"></script>
</body>
</html>